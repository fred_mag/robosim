﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace RobotSimulator.UnitTests
{
    /// <summary>
    /// Unit tests for <see cref="RobotCommander"/>.
    /// </summary>
    [TestClass]
    public class RobotCommanderTests
    {
        /// <summary>
        /// Test providing the correct upper case strings to the direction parser.
        /// </summary>
        [TestMethod]
        public void ParseDirectionFromString_CorrectStrings_ReturnsTrue()
        {
            RobotCommander robotCommand = new RobotCommander();

            foreach (Direction d in Enum.GetValues(typeof(Direction)))
            {
                var result = robotCommand.ParseDirectionFromString(d.ToString());

                Assert.IsTrue(result.Item1);
            }
        }

        /// <summary>
        /// Test providing the correct lower case strings to the direction parser.
        /// </summary>
        [TestMethod]
        public void ParseDirectionFromString_CorrectLowercaseStrings_ReturnsTrue()
        {
            RobotCommander robotCommand = new RobotCommander();

            foreach (Direction d in Enum.GetValues(typeof(Direction)))
            {
                var result = robotCommand.ParseDirectionFromString(d.ToString().ToLower());

                Assert.IsTrue(result.Item1);
            }
        }

        /// <summary>
        /// Test providing an incorrect strings to the direction parser.
        /// </summary>
        [TestMethod]
        public void ParseDirectionFromString_IncorrectString_ReturnsFalse()
        {
            RobotCommander robotCommand = new RobotCommander();

            string incorrectInput = "sERt";

            var result = robotCommand.ParseDirectionFromString(incorrectInput);

            Assert.IsFalse(result.Item1);
        }
    }
}
