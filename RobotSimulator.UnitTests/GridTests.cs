﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace RobotSimulator.UnitTests
{
    /// <summary>
    /// Unit tests for <see cref="Grid"/>.
    /// </summary>
    [TestClass]
    public class GridTests
    {
        /// <summary>
        /// Testing the grid validator with coordinates inside the grid.
        /// </summary>
        [TestMethod]
        public void ValidateCoordinates_Inside5x5Grid_ReturnsTrue()
        {
            Grid grid = new Grid();
            var rand = new Random();
            bool result;
            for (int i = 0; i < 1000; i++)
            {
                int posX = rand.Next(0, 5);
                int posY = rand.Next(0, 5);
                result = grid.ValidateCoordinates(posX, posY);

                Assert.IsTrue(result);
            }
        }

        /// <summary>
        /// Testing the grid validator with coordinates outside the grid.
        /// </summary>
        [TestMethod]
        public void ValidateCoordinates_Outside5x5Grid_ReturnsFalse()
        {
            Grid grid = new Grid();
            var rand = new Random();
            bool result;
            for (int i = 0; i < 1000; i++)
            {
                int posX = rand.Next(6, 11);
                int posY = rand.Next(6, 11);
                result = grid.ValidateCoordinates(posX, posY);

                Assert.IsFalse(result);
            }
        }
    }
}
