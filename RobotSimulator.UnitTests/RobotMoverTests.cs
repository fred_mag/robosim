﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace RobotSimulator.UnitTests
{
    /// <summary>
    /// Unit tests for <see cref="RobotMover"/>.
    /// </summary>
    [TestClass]
    public class RobotMoverTests
    {
        /// <summary>
        /// Testing that valid PLACE parameters will alter
        /// the <see cref="RobotStatus"/>.
        /// </summary>
        [TestMethod]
        public void PlaceRobot_validPLACE_ReturnsTrue()
        {
            RobotMover robotMover = new RobotMover();
            robotMover.PlaceRobot(1, 2, Direction.SOUTH);
            Console.WriteLine(RobotStatus.PositionX);

            Assert.IsTrue(RobotStatus.PositionX == 1);
            Assert.IsTrue(RobotStatus.PositionY == 2);
            Assert.IsTrue(RobotStatus.DirectionY == Direction.SOUTH);
        }

        /// <summary>
        /// Testing that invalid PLACE parameters will not alter
        /// the <see cref="RobotStatus"/>.
        /// </summary>

        [TestMethod]
        public void PlaceRobot_invalidPLACE_ReturnsTrue()
        {
            RobotMover robotMover = new RobotMover();
            robotMover.PlaceRobot(1, 23245, Direction.SOUTH);
            Console.WriteLine(RobotStatus.PositionX);

            Assert.IsFalse(RobotStatus.PositionX == 1);
            Assert.IsFalse(RobotStatus.DirectionY == Direction.SOUTH);
        }

        /// <summary>
        /// Testing the MOVE command up 4 moves then east 4 moves.
        /// </summary>
        [TestMethod]
        public void MoveForward_MOVE_InsideGrid_ReturnsTrue()
        {
            RobotStatus.IsPlaced = true;
            RobotStatus.DirectionY = Direction.NORTH;
            RobotMover robotMover = new RobotMover();
            for (int i = 0; i < 4; i++)
            {
                robotMover.MoveForward();
            }
            Assert.IsTrue(RobotStatus.PositionY == 4);
            RobotStatus.DirectionY = Direction.EAST;
            for (int i = 0; i < 4; i++)
            {
                robotMover.MoveForward();
            }
            Assert.IsTrue(RobotStatus.PositionX == 4);
        }

        /// <summary>
        /// Testing the MOVE command north 100 moves then east 100 moves.
        /// </summary>
        [TestMethod]
        public void MoveForward_MOVE_BeyondGrid_ReturnsTrue()
        {
            RobotStatus.IsPlaced = true;
            RobotStatus.DirectionY = Direction.NORTH;
            RobotMover robotMover = new RobotMover();
            for (int i = 0; i < 100; i++)
            {
                robotMover.MoveForward();
            }
            Assert.IsTrue(RobotStatus.PositionY == 4);
            RobotStatus.DirectionY = Direction.EAST;
            for (int i = 0; i < 100; i++)
            {
                robotMover.MoveForward();
            }
            Assert.IsTrue(RobotStatus.PositionX == 4);
        }

        /// <summary>
        /// Tests the RIGHT rotation command.
        /// </summary>
        [TestMethod]
        public void RotateRobot_RIGHT_ReturnsTrue()
        {
            RobotStatus.IsPlaced = true;
            RobotStatus.DirectionY = Direction.NORTH;
            RobotMover robotMover = new RobotMover();
            for (int i = 0; i < 7; i++)
            {
                robotMover.RotateRobot(true);
            }
            Assert.IsTrue(RobotStatus.DirectionY == Direction.WEST);
        }

        /// <summary>
        /// Tests the LEFT rotation command.
        /// </summary>
        [TestMethod]
        public void RotateRobot_LEFT_ReturnsTrue()
        {
            RobotStatus.IsPlaced = true;
            RobotStatus.DirectionY = Direction.NORTH;
            RobotMover robotMover = new RobotMover();
            for (int i = 0; i < 7; i++)
            {
                robotMover.RotateRobot(false);
            }
            Assert.IsTrue(RobotStatus.DirectionY == Direction.EAST);
        }
    }
}
