﻿using System.Collections.Generic;
using System.IO;


namespace RobotSimulator
{
    /// <summary>
    /// Used to read test text files lines from app directory.
    /// Test files contain command lines that control the robot.
    /// </summary>
    public class TextFileCommandReader
    {
        private List<string> commandLines = new List<string>();
        /// <summary>
        /// The command lines that can be used by <see cref="RobotCommander"/>.
        /// </summary>
        public List<string> CommandLines { get => commandLines; }

        /// <summary>
        /// Reads a command text file line by line. The lines are added to <see cref="CommandLines"/>.
        /// </summary>
        /// <param name="filePath"></param>
        public void ReadFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                commandLines.Clear();
                try
                {
                    using (StreamReader reader = new StreamReader(filePath))
                    {
                        int counter = 0;
                        string line;

                        while ((line = reader.ReadLine()) != null)
                        {
                            commandLines.Add(line);
                            counter++;
                        }
                        TextDisplayer.Informing($"Read {counter} commands from file.");
                    }
                }
                catch (IOException e)
                {
                    //Could not read the file
                    TextDisplayer.Warning("Could not read the text file:");
                    TextDisplayer.Warning(e.Message);
                }
            }
            else
            {
                //Text file not found
                TextDisplayer.Warning("Missing file: " + filePath);
            }
        }
    }
}