﻿namespace RobotSimulator
{
    /// <summary>
    /// Used to describe a 2D grid area with x,y coordinates and
    /// to check if a coordinate is inside the grid.
    /// The origin is the bottom left corner.
    /// </summary>
    public class Grid
    {
        public int originX = 0;
        public int originY = 0;

        public int rowCount = 5;
        public int columnCount = 5;

        /// <summary>
        /// Used to check if a coordinate is inside the grid.
        /// </summary>
        public bool ValidateCoordinates(int x, int y)
        {
            if (x >= originX && x <= rowCount - originX - 1 && y >= originY && y <= columnCount - originY - 1)
            {
                //Coordinate is inside the grid area
                return true;
            }
            else
            {
                //Coordinate is outside the grid area
                return false;
            }
        }
    }
}