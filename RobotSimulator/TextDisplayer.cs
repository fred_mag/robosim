﻿using System;


namespace RobotSimulator
{
    /// <summary>
    /// Text color coding and a text line separator.
    /// </summary>
    public static class TextDisplayer
    {
        public static void Informing(string message)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void Confirming(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void Reporting(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void Warning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void Ignoring(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        /// <summary>
        /// A text line separator for the command prompt.
        /// </summary>
        /// <param name="character"></param>
        public static void CharFillConsoleWidth(char character)
        {
            string line = "".PadRight(Console.BufferWidth, character);
            Console.WriteLine(line);
        }
    }
}