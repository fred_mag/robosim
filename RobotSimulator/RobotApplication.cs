﻿namespace RobotSimulator
{
    /// <summary>
    /// Application startpoint. Initilizes and instantiates objects that give the
    /// desired flow of the app. 
    /// </summary>
    public class RobotApplication
    {
        public TextFileCommandReader textFileCommandReader = new TextFileCommandReader();
        public UI ui = new UI();
        public RobotCommander robotCommander = new RobotCommander();

        /// <summary>
        /// Initiliazing and welcoming.
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            RobotApplication robotApplication = new RobotApplication();

            TextDisplayer.Informing("Welcome to Robot Simulator 1.0.0-alpha. \n");

            TextFileManager.InitializeFilesList();

            robotApplication.AppLoop();
        }

        /// <summary>
        /// This is where the user ends up after the initial welcome and tesrrun.
        /// </summary>
        private void AppLoop()
        {
            robotCommander.robotMover.UnplaceRobot();

            ui.SelectTestFile(TextFileManager.FileInfos, textFileCommandReader.ReadFile);

            robotCommander.BatchRunCommandList(textFileCommandReader.CommandLines);

            ui.RunDialoguePrompt(AppLoop);
        }
    }
}
