﻿namespace RobotSimulator
{
    /// <summary>
    /// Used for easy access to filenames coupled with paths.
    /// </summary>
    public struct FileInfo
    {
        public string FilePath;
        public string FileName;

        public FileInfo(string filePath, string fileName)
        {
            FilePath = filePath;
            FileName = fileName;
        }
    }
}