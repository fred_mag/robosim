﻿namespace RobotSimulator
{
    /// <summary>
    /// Used to track the position, rotation, and placed status of the robot.
    /// </summary>
    public static class RobotStatus
    {
        public static bool IsPlaced;
        public static int PositionX;
        public static int PositionY;
        public static Direction DirectionY;
        
        /// <summary>
        /// This is the data proivided from a REPORT command.
        /// </summary>
        public static string ReportStatus { 
            get { return string.Format("Output: {0},{1},{2}",
                PositionX, PositionY, DirectionY.ToString()); } }        
    }
}
