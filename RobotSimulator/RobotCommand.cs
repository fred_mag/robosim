﻿namespace RobotSimulator
{
    /// <summary>
    /// The commands available for controlling the robot.
    /// </summary>
    public enum RobotCommand
    {
        PLACE,
        MOVE,
        LEFT,
        RIGHT,
        REPORT
    }
}