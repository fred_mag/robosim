﻿using System;
using System.Collections.Generic;


namespace RobotSimulator
{
    /// <summary>
    /// Receives lines of string robot commands and parses them to <see cref="RobotCommand"s/>.
    /// The commands are used to invoke <see cref="RobotMover"/> methods that displaces the robot.
    /// Also parses the parameters of the PLACE commmand.
    /// </summary>
    public class RobotCommander
    {
        public RobotMover robotMover = new RobotMover();

        /// <summary>
        /// Receives a list of string commands and passes 
        /// them on to the  <see cref="EvaluateCommandInput"/>.
        /// </summary>
        public void BatchRunCommandList(List<string> commandLines)
        {
            for (int i = 0; i < commandLines.Count; i++)
            {
                EvaluateCommandInput(commandLines[i]);
            }
        }

        /// <summary>
        /// Checks if a command is valid and, if so, passes it on to <see cref="RunCommand"/>. 
        /// Ignores commands, except PLACE, until robot is placed.
        /// </summary>
         public void EvaluateCommandInput(string input)
        {
            input = input.ToUpper();
            foreach (RobotCommand command in Enum.GetValues(typeof(RobotCommand)))
            {
                if (input.Contains(command.ToString()))
                {
                    if (!RobotStatus.IsPlaced && command != RobotCommand.PLACE)
                        return;

                    if (command == RobotCommand.PLACE)
                    {
                        RunCommand(command, input);
                        return;
                    }
                    else
                    {
                        RunCommand(command);
                        return;
                    }
                }
            }
            //Not a valid command
            TextDisplayer.Warning("Unrecognized command detected: " + input);
        }


        /// <summary>
        /// Invokes the appropriate method of the <see cref="robotMover"/>.
        /// The PLACE command will be sent for further parsing of its parameters.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="placeCommand">optional parameters used by the PLACE command</param>
        public void RunCommand(RobotCommand command, string placeCommand = "")
        {
            switch (command)
            {
                case RobotCommand.PLACE:
                    ExtractPlaceParams(placeCommand);
                    break;
                case RobotCommand.MOVE:
                    robotMover.MoveForward();
                    break;
                case RobotCommand.LEFT:
                    robotMover.RotateRobot(false);
                    break;
                case RobotCommand.RIGHT:
                    robotMover.RotateRobot(true);
                    break;
                case RobotCommand.REPORT:
                    string report = RobotStatus.ReportStatus;
                    TextDisplayer.Reporting(RobotStatus.ReportStatus + " - Reporting status");
                    break;
                default:
                    TextDisplayer.Warning("Unrecognized command detected.");
                    break;
            }
        }

        /// <summary>
        /// Extracts the PLACE parameters and checks if they are valid
        /// before invoking <see cref="RunPlaceCommand(int, int, Direction)"/>.
        /// </summary>
        /// /// <param name="commandData">parameter data to be parsed</param>
        public void ExtractPlaceParams(string commandData)
        {
            string parameters = commandData.Replace("PLACE ", "");
            string[] parametersArray = parameters.Split(',');

            int posX;
            if (!Int32.TryParse(parametersArray[0], out posX))
            {
                TextDisplayer.Warning("Unrecognized PLACE parameter for X: " + parametersArray[0]);
                return;
            }
            int posY;
            if (!Int32.TryParse(parametersArray[1], out posY))
            {
                TextDisplayer.Warning("Unrecognized PLACE parameter for Y: " + parametersArray[1]);
                return;
            }

            string direction = parametersArray[2];
            (bool isValid, Direction directionEnum) directionData = ParseDirectionFromString(direction);
            if (directionData.isValid)
            {
                RunPlaceCommand(posX, posY, directionData.directionEnum);
            }
            else
            {
                TextDisplayer.Warning("Unrecognized PLACE parameter for direction: " + parametersArray[2]);
            }
        }

        /// <summary>
        /// Receives a direction in string format and converts and validates
        /// it to a <see cref="Direction"/>.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public (bool, Direction) ParseDirectionFromString(string direction)
        {
            foreach (Direction d in Enum.GetValues(typeof(Direction)))
            {
                if (direction.ToUpper().Contains(d.ToString()))
                {                    
                    return (true, d);
                }
            }
            //Failed to parse direction data from command input.
            return (false, 0);
        }

        /// <summary>
        /// Wrapper for <see cref="RobotMover.PlaceRobot"/>.
        /// </summary>
        public void RunPlaceCommand(int posX, int posY, Direction direction)
        {
            robotMover.PlaceRobot(posX, posY, direction);
        }
    }   
}