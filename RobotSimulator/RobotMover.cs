﻿namespace RobotSimulator
{
    /// <summary>
    /// Moves and rotates the robot if it has been placed.
    /// </summary>
    public class RobotMover
    {
        // Degrees of rotation for LEFT/RIGHT commands.
        private int rotationDegrees = 90;
        //The ammount of units the robot will move forward following a MOVE command. 
        private int moveUnitSize = 1;
        private Grid grid = new Grid();

        /// <summary>
        /// Places the robot on a specific coordinate on the grid and facing a certain direction.
        /// </summary>
        /// <param name="direction">The robots facing direction.</param>
        public void PlaceRobot(int x, int y, Direction direction)
        {
            if (grid.ValidateCoordinates(x, y))
            {
                SetDirection(direction);
                SetPosition(x, y);

                if (!RobotStatus.IsPlaced)
                {
                    TextDisplayer.Confirming("The robot was placed on the table.");
                    RobotStatus.IsPlaced = true;
                }
                else
                {
                    TextDisplayer.Confirming("The robot was placed to a new location.");
                }
            }
            else
            {
                //Not a valid placement
                TextDisplayer.Ignoring("Robot placement outside of table denied.");
            }    
        }

        /// <summary>
        /// Sets the direction of the robot.
        /// </summary>
        /// <param name="direction"></param>
        public void SetDirection(Direction direction)
        {
            RobotStatus.DirectionY = direction;
        }

        /// <summary>
        /// Removes the robot from the grid causing it to ignore all
        /// following commands except for the PLACE command.
        /// </summary>
        public void UnplaceRobot()
        {
            RobotStatus.IsPlaced = false;
        }

        /// <summary>
        /// Sets the coordinate location for the robot.
        public void SetPosition(int x, int y)
        {
            RobotStatus.PositionX = x;
            RobotStatus.PositionY = y;
        }

        /// <summary>
        /// Moves the robot forward by one unit, unless it's at the edge of the grid.
        /// </summary>
        public void MoveForward()
        {
            if (!RobotStatus.IsPlaced)
                return;

            int desiredDestinationX = RobotStatus.PositionX;
            int desiredDestinationY = RobotStatus.PositionY;

            switch (RobotStatus.DirectionY)
            {
                case Direction.NORTH:
                    desiredDestinationY += moveUnitSize;
                    break;
                case Direction.SOUTH:
                    desiredDestinationY -= moveUnitSize;
                    break;
                case Direction.EAST:
                    desiredDestinationX += moveUnitSize;
                    break;
                case Direction.WEST:
                    desiredDestinationX -= moveUnitSize;
                    break;
                default:
                    break;
            }

            if (grid.ValidateCoordinates(desiredDestinationX, desiredDestinationY))
            {
                //Valid move
                SetPosition(desiredDestinationX, desiredDestinationY);
                TextDisplayer.Confirming("The robot moved forward.");
            }
            else
            {
                //Invalid move
                TextDisplayer.Ignoring("The robot can't move past the table edges.");
            }
        }


        /// <summary>
        /// Rotates the robot by a value of <see cref="rotationDegrees"/> to 
        /// the robots local left or right.
        /// </summary>
        /// <param name="turnRight">If true rotates right, if not left.</param>
        public void RotateRobot(bool turnRight)
        {
            if (!RobotStatus.IsPlaced)
                return;

            Direction newDirection;
            if (turnRight)
            {
                newDirection = (int)RobotStatus.DirectionY + rotationDegrees > (int)Direction.WEST ? 
                                                Direction.NORTH : (Direction)((int)RobotStatus.DirectionY + rotationDegrees);
                
            }
            else
            {
                newDirection = (int)RobotStatus.DirectionY - rotationDegrees < (int)Direction.NORTH ?
                                                Direction.WEST : (Direction)((int)RobotStatus.DirectionY - rotationDegrees);
                
            }

            RobotStatus.DirectionY = newDirection;

            string turnDirection = turnRight == true ? "right" : "left";
            TextDisplayer.Confirming($"The robot rotated to the {turnDirection}.");
        }
    }
}
