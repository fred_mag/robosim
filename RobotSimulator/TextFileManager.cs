﻿using System.Collections.Generic;
using System.IO;


namespace RobotSimulator
{
    /// <summary>
    /// Gathers the path and names of all txt files found in the 
    /// <see cref=".AppDomain.CurrentDomain.BaseDirectory + @"TestFiles\ "/> directory.
    /// </summary>
    public static class TextFileManager
    {
        private static List<FileInfo> fileInfos = new List<FileInfo>();
        //The list of available txt files paths and names.
        public static List<FileInfo> FileInfos { get => fileInfos; }

        /// <summary>
        /// This has to be initilalized before trying to read <see cref="FileInfos"/>.
        /// </summary>
        public static void InitializeFilesList()
        {
            string filePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory + @"TestFiles\");
            fileInfos.Clear();
            foreach (var path in Directory.GetFiles(filePath, "*.txt"))
            {
                string fileName = Path.GetFileName(path);
                fileInfos.Add(new FileInfo(path, fileName));
            }
        }
    }
}