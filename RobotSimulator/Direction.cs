﻿namespace RobotSimulator
{
    /// <summary>
    /// The available directions and their values in degrees,
    /// which can be used for rotation calculations.
    /// </summary>
    public enum Direction
    {
        NORTH = 0,
        EAST = 90,
        SOUTH = 180,
        WEST = 270
    }
}
