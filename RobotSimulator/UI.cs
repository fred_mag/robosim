﻿using System;
using System.Collections.Generic;


namespace RobotSimulator
{
    /// <summary>
    /// Contains the prompt logic for user interactions in the app.
    /// </summary>
    public class UI
    {
        /// <summary>
        /// Displays the test files selection prompt and triggers <see cref="filePathIndex"/>
        /// to forward the path of the selected test file.
        /// The user selects a test file by pressing a corresponding number.
        /// </summary>
        /// <param name="fileInfos">All available files paths coupled with their names.</param>
        /// <param name="filePath">The path of the selected file.</param>
        public void SelectTestFile(List<FileInfo> fileInfos, Action<string> filePath)
        {
            TextDisplayer.Informing("Select a test file to start the simulator: ");

            int count = 0;
            foreach (var test in fileInfos)
            {
                TextDisplayer.Informing($"Press {count} to run {test.FileName}");                
                count++;
            }

            int selection;
            while (!int.TryParse(Console.ReadKey(true).KeyChar.ToString(), 
                    out selection) || selection >= fileInfos.Count)
            {
                if (selection >= fileInfos.Count)
                {
                    TextDisplayer.Warning("Not a valid selection.");
                }
                else
                {
                    TextDisplayer.Warning("Please enter a number.");
                }
            }

            if (selection >= 0 && selection < TextFileManager.FileInfos.Count)
            {
                TextDisplayer.Confirming($"Selected {fileInfos[selection].FileName}");
                filePath(fileInfos[selection].FilePath);
            }
        }    

        /// <summary>
        /// Displays the run test or exit app promt after the first test is completed.
        /// </summary>
        /// <param name="selectTestFile">Used to trigger the "run new" behaviour.</param>
        public void RunDialoguePrompt(Action selectTestFile)
        {
            TextDisplayer.Informing($"Press R to run a new simulation or press X to quit.");

            ConsoleKey response;
            do
            {
                response = Console.ReadKey(true).Key;
            } 
            while (response != ConsoleKey.R && response != ConsoleKey.X);

            if (response == ConsoleKey.R)
            {
                TextDisplayer.CharFillConsoleWidth('_');
                selectTestFile();
            }
            else if (response == ConsoleKey.X)
            {
                Environment.Exit(0);
            }
        }
    }
}